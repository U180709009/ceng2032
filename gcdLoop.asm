.data 
    str1: .asciiz "Enter an integer: "
    str2: .asciiz "\nEnter another integer: "
    str3: .asciiz "\nThe greatest common divisor is: "

.text
main:
#Request first integer from user
    #print str1
    li $v0, 4
    la $a0, str1
    syscall

    #getting an input from user
    li $v0, 5
    syscall

    #store the result in $t0
    move $t0, $v0

#Request second integer from user
    #print str2
    li $v0, 4
    la $a0, str2
    syscall

    #getting an another input from user
    li $v0, 5
    syscall

    #store the result in $t0
    move $t1, $v0

    #creating 2 parameters
     add $a1, $zero, $t0
    add $a2, $zero, $t1


    addi $sp, $sp, -12
    sw $a1, 8($sp)
    sw $a2, 4($sp)
    #calling function
    move $t0, $a1
    move $t1, $a2
    jal gcdloop

    #display str3
    li $v0, 4
    la $a0, str3
    syscall

    #getting result from godLoop(v1)
    li $v0, 1
    add $a0, $zero, $v1
    syscall

    #to finish main
    li $v0, 10
    syscall

gcdloop:
    sw $ra, 0($sp)
    
    slt $t3, $t0, $t1
    beq $t3, 0, gcd2
    #a=5, b=6
    add $t1, $t1, $t0  #a=6+5=11
    sub $t0, $t1, $t0  #b=11-6=5
    sub $t1, $t1, $t0  #a=a-b

gcd2:

    div $t0, $t1
    mfhi $t5
    beq $t5 0 exit
    mfhi $t0
    j gcdloop

exit:
    add $v1, $zero, $t1
    
    lw $a1, 8($sp)
    lw $a2, 4($sp)
    lw $ra, 0($sp)
    addi $sp, $sp, 12
    
    jr $ra
    syscall