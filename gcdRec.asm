.data 
	str1: .asciiz "Enter an integer: "
	str2: .asciiz "\nEnter another integer: "
	str3: .asciiz "\nThe greatest common divisor is: "

.text
main:
#Request first integer from user
	#print integer1
	li $v0, 4
	la $a0, str1
	syscall
	
	#get the user's integer1
	li $v0, 5
	syscall
	
	#store the integer1 in $t0
	move $t0, $v0
	
#Request second integer from user
	#print integer2
	li $v0, 4
	la $a0, str2
	syscall
	
	#get the user's integer2
	li $v0, 5
	syscall
	
	#store the integer2 in $t1
	move $t1, $v0
	
	#creating 2 parameters
 	add $a1, $zero, $t0
	add $a2, $zero, $t1
	
	
	
	#calling function
	move $t0, $a1
	move $t1, $a2
	jal gcdRec
	sw $ra, 0($sp)

	
	#print str3
	li $v0, 4
	la $a0, str3
	syscall

	#get result from gcdRec in  v1
	li $v0, 1
	move $a0, $v1
	syscall

	#end of program
	li $v0, 10
	syscall

gcdRec:
	addi $sp, $sp, -12
	sw $a1, 8($sp)
	sw $a2, 4($sp)
	sw $ra, 0($sp)
	
	slt $t3, $t0, $t1
	beq $t3, 1, lessthan
	
	beq $t1, $zero, return
	
	div $t0, $t1
	addi $t0, $t1, 0
	mfhi $t1
	
	j gcdRec
	
return:
	add $v1, $t0, $zero
	
	addi $sp, $sp, 12
	lw $ra, 0($sp)
		
	jr $ra
	syscall
lessthan:
    #a=5, b=6
    	move $t0, $a1
	move $t1, $a2
	
    add $t1, $t1, $t0  #a=6+5=11
    sub $t0, $t1, $t0  #b=11-6=5
    sub $t1, $t1, $t0  #a=a-b
    
    	j gcdRec
